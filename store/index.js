import Vuex from 'vuex'
import axios from 'axios'
import _ from 'lodash'

const createStore = () => {
  return new Vuex.Store({
    state: {
      organization: null,
      errors: []
    },
    mutations: {
      GET_ORGANIZATION_SUCCESS (state, organization) {
        state.errors = []
        state.organization = organization
      },
      GET_ORGANIZATION_FAILURE (state, error) {
        state.organization = null
        state.errors.push(error)
      }
    },
    actions: {
      async nuxtServerInit ({commit}, {env, params}) {
        const url = env.BASE_URL + '/api/o/' + params.aliasOrganization
        try {
          const {data} = await axios.get(url)
          const organization = data.organization
          if (organization) {
            commit('GET_ORGANIZATION_SUCCESS', organization)
          } else {
            commit('GET_ORGANIZATION_FAILURE', 'ORGANIZATION IS UNDEFINED')
          }
        } catch (e) {
          commit('GET_ORGANIZATION_FAILURE', e.toString())
          console.log(e)
        }
      }
    },
    getters: {
      organization (state) {
        return state.organization
      },
      appUrl () {
        return process.server ? process.env.BASE_URL : process.env.APP_URL
      }
    }
  })
}

export default createStore
