# publiko-ui

> UI for Publiko app

## Development Setup

Create an `.env` in root directory with the following content:

```
APP_URL=http://127.0.0.1:3000
REVAS_PUBLIC_API_URL=XXXXXX
GRECAPTCHA_SITE_KEY=XXXXX
GRECAPTCHA_SECRET_KEY=XXXXX
```

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
