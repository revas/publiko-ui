var fs = require('fs')
const axios = require('axios')
const _ = require('lodash')
const bodyParser = require('body-parser')

require.extensions['.md'] = function (module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8')
}

const express = require('express')
const { Nuxt, Builder } = require('nuxt')
const marked = require('marked')

const app = express()
app.use(bodyParser.json())
const host = process.env.HOST || '127.0.0.1'
const port = process.env.PORT || 3000

app.set('port', port)

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }
  
  app.get('/api/o/:alias', async (req, res) => {
    const body = {}
    const url = process.env.REVAS_PUBLIC_API_URL + '/publiko.GetInfo/'
      try {
        const {data} = await axios.post(url, {
          organizations: [{
            alias: req.params.alias
          }]
        })
        body.organization = _.get(data, ['organizations', '0'])
      } catch (e) {
        body.error = e.toString()
      }
      res.send(body)
  })

  const getPosts = async (organizationId, aliasBlog) => {
    const { data } = await axios.post(process.env.REVAS_PUBLIC_API_URL + '/publiko.QueryPosts/', {
      organizations: [{
        id: organizationId,
        blogs: [{
          domain: "default"
        }]
      }]
    })
    return _.get(data, ['organizations', '0', 'blogs', '0', 'posts'])
  }

  app.get('/api/o/:id/blog/:aliasBlog/', async (req, res) => {
    let body = {}
    try {
      const posts = await getPosts( req.params.id, req.params.aliasBlog )
      body.posts = _.reject(posts, { 'title': 'Statuto' })
    } catch (e) {
      body.errors.push(e)
      console.error(e)
    }
    res.send(body)
  })

  app.get('/api/o/:id/blog/:aliasBlog/p/:slug', async (req, res) => {
    let body = {}
    try {
      const posts = await getPosts( req.params.id, req.params.aliasBlog)
      const post = _.find(posts, (p) => {
        return p.slug === req.params.slug
      })
      const { data } = await axios.post(process.env.REVAS_PUBLIC_API_URL + '/publiko.GetPosts/', {
        organizations: [{
          id: req.params.id,
          blogs: [{
            domain: 'default',
            posts: [{
              id: post.id
            }]
          }]
        }]
      })
      let content = _.get(data, ['organizations', '0', 'blogs', '0', 'posts', '0', 'content'])
      post.content = marked(content, { sanitize: true })
      body.post = post
    } catch (e) {
      body.error = e.toString()
    }
    res.send(body)
  })

  app.post('/api/verifyHuman', async (req, res) => {
    const token = req.body.token
    var secretKey = process.env.GRECAPTCHA_SECRET_KEY
    var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + token
    try {
      let { data } = await axios.post(verificationUrl)
      res.send(data)
    } catch (e) {
      console.log(e)
    }
  })

  app.post('/api/draftMembers', async (req, res) => {
    const body = {}
    const payload = req.body
    const url = process.env.REVAS_PUBLIC_API_URL + '/publiko.DraftMembers/' 
    try {
      let { data } = await axios.post(url, payload)
      body.member = _.get(data, ['organizations', '0', 'members', '0'])
      const error = _.get(data, ['organizations', '0', 'error'])
      if (error) {
        body.errors.push(error)
      }
    } catch (e) {
      body.errors.push(e)
      console.log(e)
    }
    res.send(body)
  })

  app.post('/api/validateTokens', async (req, res) => {
    const body = {
      response: {},
      errors: []
    }
    const payload = req.body
    const url = process.env.REVAS_PUBLIC_API_URL + '/publiko.ValidateTokens/' 
    try {
      let { data } = await axios.post(url, payload)
      body.response = data
      const error = _.get(data, ['organizations', '0', 'error'])
      if (error) {
        body.errors.push(error)
      }
    } catch (e) {
      body.errors.push(e)
      console.log(e)
    }
    res.send(body)
  })

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  console.log('Server listening on http://' + host + ':' + port) // eslint-disable-line no-console
}
start()
