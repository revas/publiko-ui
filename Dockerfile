FROM mhart/alpine-node:10 as builder
ARG APP_URL
RUN echo ${APP_URL}
WORKDIR /usr/src
COPY package.json yarn.lock /usr/src/
RUN yarn
COPY . .
RUN yarn build && yarn --production

FROM mhart/alpine-node:base-10
WORKDIR /usr/src
ENV NODE_ENV="production"
ENV HOST 0.0.0.0
COPY --from=builder /usr/src .
EXPOSE 3000
CMD ["node", "server/index.js"]
